
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkAlert extends SkElement {

    get cnSuffix() {
        return 'alert';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get type() {
        return this.getAttribute('type') || 'success';
    }

    close() {
        this.impl.onClose();
    }
}
