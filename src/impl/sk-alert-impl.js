

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';


export class SkAlertImpl extends SkComponentImpl {

    get suffix() {
        return 'alert';
    }

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    get slot() {
        return this.comp.el.querySelector('slot');
    }

    dumpState() {
        return {
            'contents': this.comp.contentsState
        }
    }

    restoreState(state) {
        this.slot.innerHTML = state.contentsState;
    }

    onClose() {
        this.comp.style.display = 'none';
    }

    bindEvents() {
        super.bindEvents();
        if (this.comp.hasAttribute('closable')) {
            this.closeBtnEl.onclick = function(event) {
                this.comp.callPluginHook('onEventStart', event);
                this.onClose(event);
                this.comp.callPluginHook('onEventEnd', event);
            }.bind(this);
        }
    }

    unbindEvents() {
        super.unbindEvents();
        this.closeBtnEl.onclick = null;
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
        if (! this.comp.hasAttribute('closable')) {
            if (this.closeBtnEl) {
                this.closeBtnEl.style.display = 'none';
            }
        }
    }
}